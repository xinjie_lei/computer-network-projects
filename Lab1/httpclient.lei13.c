#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#define MAXLINE 256

int open_clientfd(char *hostname, int port)
{
  int clientfd;
  struct hostent *hp;
  struct sockaddr_in serveraddr;
  typedef struct sockaddr SA;

  if((clientfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
      return -1;
    }

  if((hp = gethostbyname(hostname)) == NULL)
    {
      return -2;
    }
  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  bcopy((char *)hp ->h_addr,
	(char *) &serveraddr.sin_addr.s_addr, hp->h_length);
  serveraddr.sin_port = htons(port);

  if(connect(clientfd, (SA *)&serveraddr, sizeof(serveraddr)) < 0)
    {
      return -1;
    }
  return clientfd;
}

int main (int argc, char ** argv)
{
  int clientfd, port;
  char *host, buf[MAXLINE];
  char *msg;
  ssize_t bytes_read;

  host = argv[1];
  port = atoi(argv[2]);
  msg = malloc(strlen(argv[3]) + 17*sizeof(char));
  sprintf(msg, "GET %s HTTP/1.0\r\n\r\n", argv[3]);

  clientfd = open_clientfd(host,port);
  if(clientfd >= 0)
    {
      if((bytes_read = write(clientfd, msg, strlen(msg))) >= 0)
	{
	  while((bytes_read = read(clientfd, buf, sizeof(buf))) > 0) 
	    {
	      fwrite(buf, bytes_read, 1, stdout);
	    }
	}	
    }
  close(clientfd);
  return 0;
} 
