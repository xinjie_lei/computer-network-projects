#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <errno.h>
#define MAXLINE 256

int open_listenfd (int port){
  int listenfd, optval = 1;
  struct sockaddr_in serveraddr;
  typedef struct sockaddr SA;
  /*Create a socket descripter*/
  if((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
    return -1;
  }
  /*Eliminate address already in use error in bind*/
  if(setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval, sizeof(int)) < 0){
    return -2;
  }

  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serveraddr.sin_port = htons((unsigned short) port);
  if(bind(listenfd, (SA *)&serveraddr, sizeof(serveraddr)) < 0){
    printf("Error code: %d", errno);
    return -3;
  }
  if(listen(listenfd, 10) < 0){
    return -4;
  }
  return listenfd;
}

void response(int connfd){
  char *tok;
  char *filename = malloc(sizeof(char) * 100);
  char req[10000], buf[MAXLINE];
  FILE * fp;
  int bytes_read;
  char dot = '.';
  if(read(connfd, req, 10000) > 0){
    tok = strtok(req, " ");
    if(strcmp(tok, "GET\0") == 0){
      tok = strtok(NULL, " ");
      if(tok[0] == '/'){
	sprintf(filename, "%c%s", dot, tok);
      }
      else {
	filename = tok;
      }
      fp = fopen(filename, "r");
      if(fp == NULL){
	if(errno == ENOENT){
	  write(connfd, "HTTP/1.0 404 Not Found\r\n\r\n", strlen("HTTP/1.0 404 Not Found\r\n\r\n"));
	}
	else if (errno == EACCES){
	  write(connfd, "HTTP/1.0 403 Forbidden\r\n\r\n", strlen("HTTP/1.0 403 Forbidden\r\n\r\n"));
	}
      }
      else
	{
	  write(connfd, "HTTP/1.0 200 OK\r\n\r\n", strlen("HTTP/1.0 200 OK\r\n\r\n"));
	  while((bytes_read = fread(buf, 1, MAXLINE, fp)) > 0)
	    {
	      write(connfd, buf, bytes_read);
	      memset(buf, 0, MAXLINE);
	    }
	  fclose(fp);
	}      
    }
  }
}

int main(int argc, char ** argv){
  int listenfd, connfd, port;
  struct sockaddr_in clientaddr;
  socklen_t clientlen;
  typedef struct sockaddr SA;
  int childpid;

  port = atoi(argv[1]);
  listenfd = open_listenfd(port);
  while(1){
    clientlen = sizeof(clientaddr);
    connfd = accept(listenfd, (SA *)&clientaddr, &clientlen);
   
    if((childpid =fork()) == 0){
      close(listenfd);
      response(connfd);
      exit(0);
    }
    close(connfd);
  }
  return 0;
}
