#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <errno.h>
#define MAXLINE 256

int open_listenfd_http (int port){
  int listenfd, optval = 1;
  struct sockaddr_in serveraddr;
  typedef struct sockaddr SA;
  /*Create a socket descripter*/
  if((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
    return -1;
  }
  /*Eliminate address already in use error in bind*/
  if(setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval, sizeof(int)) < 0){
    return -2;
  }

  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serveraddr.sin_port = htons((unsigned short) port);
  if(bind(listenfd, (SA *)&serveraddr, sizeof(serveraddr)) < 0){
    printf("Error code: %d", errno);
    return -3;
  }
  if(listen(listenfd, 10) < 0){
    return -4;
  }
  return listenfd;
}

void response_http(int connfd){
  char *tok;
  char *filename = malloc(sizeof(char) * 100);
  char req[10000], buf[MAXLINE];
  FILE * fp;
  int bytes_read;
  char dot = '.';
  if(read(connfd, req, 10000) > 0){
    tok = strtok(req, " ");
    if(strcmp(tok, "GET\0") == 0){
      tok = strtok(NULL, " ");
      if(tok[0] == '/'){
	sprintf(filename, "%c%s", dot, tok);
      }
      else {
	filename = tok;
      }
      fp = fopen(filename, "r");
      if(fp == NULL){
	if(errno == ENOENT){
	  write(connfd, "HTTP/1.0 404 Not Found\r\n\r\n", strlen("HTTP/1.0 404 Not Found\r\n\r\n"));
	}
	else if (errno == EACCES){
	  write(connfd, "HTTP/1.0 403 Forbidden\r\n\r\n", strlen("HTTP/1.0 403 Forbidden\r\n\r\n"));
	}
      }
      else
	{
	  write(connfd, "HTTP/1.0 200 OK\r\n\r\n", strlen("HTTP/1.0 200 OK\r\n\r\n"));
	  while((bytes_read = fread(buf, 1, MAXLINE, fp)) > 0)
	    {
	      write(connfd, buf, bytes_read);
	      memset(buf, 0, MAXLINE);
	    }
	  fclose(fp);
	}      
    }
  }
}

int open_listenfd_udp (int port){
  int listenfd, optval = 1;
  struct sockaddr_in serveraddr;
  typedef struct sockaddr SA;

  /*Create a socket descripter*/
  if((listenfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
    return -1;
  }
  /*Eliminate address already in use error in bind*/
  if(setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval, sizeof(int)) < 0){
    return -2;
  }

  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serveraddr.sin_port = htons((unsigned short) port);
  if(bind(listenfd, (SA *)&serveraddr, sizeof(serveraddr)) < 0){
    printf("Error code: %d", errno);
    return -3;
  }
  return listenfd;
}

int main(int argc, char ** argv){
  int listenfd_http, listenfd_udp, connfd_http, port_http, port_udp;
  struct sockaddr_in clientaddr_http, clientaddr_udp;
  socklen_t clientlen_http, clientlen_udp;
  typedef struct sockaddr SA;
  int childpid, bytes_read, maxfdp1;
  long seq;
  char buf[68];
  fd_set rfds;
  int retval;

  port_http = atoi(argv[1]);
  port_udp = atoi(argv[2]);
  listenfd_http = open_listenfd_http(port_http);
  listenfd_udp = open_listenfd_udp(port_udp);
  while(1){
    //set up select
    FD_ZERO(&rfds);
    FD_SET(listenfd_http, &rfds);
    FD_SET(listenfd_udp, &rfds);
    maxfdp1 = (listenfd_http > listenfd_udp) ? listenfd_http + 1 : listenfd_udp +1; 
    retval = select(maxfdp1, &rfds, NULL, NULL, NULL);

    //deal with http
    if(FD_ISSET(listenfd_http, &rfds)){
      clientlen_http = sizeof(clientaddr_http);
      connfd_http = accept(listenfd_http, (SA*)&clientaddr_http, &clientlen_http);
      if((childpid =fork()) == 0){
	close(listenfd_http);
	response_http(connfd_http);
	exit(0);
      }
      close(connfd_http); 
    }
    else if(FD_ISSET(listenfd_udp, &rfds)){
      clientlen_udp = sizeof(struct sockaddr);
      bytes_read = recvfrom(listenfd_udp, buf, sizeof(buf), 0, (struct sockaddr*)&clientaddr_udp, &clientlen_udp);
      seq = ntohl(*((uint32_t *) buf));
      seq += 1;
      *(uint32_t *)buf = htonl(seq);
      //*(uint32_t *)buf += 1;
      bytes_read = sendto(listenfd_udp, buf, bytes_read, 0, (struct sockaddr*) &clientaddr_udp, clientlen_udp);
    }
  }
  return 0;
}
