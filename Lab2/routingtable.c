#include "ne.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct route_entry routingTable[MAX_ROUTERS];
int numRoutes;

void InitRoutingTbl (struct pkt_INIT_RESPONSE * InitResponse, int myID)
{
  memset(routingTable, 0, sizeof(routingTable));
  int i = 0;
  routingTable[0].dest_id = myID;
  routingTable[0].next_hop = myID;
  routingTable[0].cost = 0;
  routingTable[0].path_len = 0;
  routingTable[0].path[0] = myID;
  //printf("Init Response: %d", InitResponse -> no_nbr);
  for(i = 1; i <= InitResponse -> no_nbr; i++)
    {
      routingTable[i].dest_id = InitResponse -> nbrcost[i-1].nbr;
      routingTable[i].next_hop = InitResponse -> nbrcost[i-1].nbr;
      routingTable[i].cost = InitResponse -> nbrcost[i-1].cost;
      routingTable[i].path_len = 1;
      routingTable[i].path[0] = InitResponse -> nbrcost[i-1].nbr;
      routingTable[i].path[1] = myID;
    }

  numRoutes = InitResponse -> no_nbr + 1; 
}

int UpdateRoutes(struct pkt_RT_UPDATE *RecvdUpdatePacket, int costToNbr, int myID)
{
  int i = 0;
  int j = 0;
  int k = 0;
  int ncost = 0;
  int cur_numRoutes = numRoutes;
  int ischange = 0;
  int inpath = 0;
  int direct = 0;
  for(i = 0; i < RecvdUpdatePacket -> no_routes; i++){
    if(RecvdUpdatePacket -> route[i].dest_id == myID && RecvdUpdatePacket -> route[i].next_hop == myID){
      direct = 1;
    }
  }
  // update routing table
  //printf("Update! sender: %d\n", RecvdUpdatePacket -> sender_id);fflush(stdout);
  //printf("direct: %d\n", direct);
  for(i = 0; i < RecvdUpdatePacket -> no_routes; i++)
    {
      //printf("recvd: ind: %d, dest_id :%d, next_hop: %d, cost: %d \n", i, RecvdUpdatePacket -> route[i].dest_id, RecvdUpdatePacket -> route[i].next_hop, RecvdUpdatePacket -> route[i].cost);fflush(stdout);
      if (RecvdUpdatePacket -> route[i].cost != 0){
	//check path
	for(k = 1; k <= RecvdUpdatePacket -> route[i].path_len; k++){
	  if(myID == RecvdUpdatePacket -> route[i].path[k]){
	    inpath = 1; // 1-in the path 0 - not in the path
	    break;
	  }
	  else{
	    inpath = 0;
	  }
	}
	//I am not in the path, update!
	if(inpath == 0){
	  if(myID == RecvdUpdatePacket -> route[i].dest_id && myID ==  RecvdUpdatePacket -> route[i].next_hop){// my neighbor tells me something about us
	    for(j = 1; j < cur_numRoutes; j++){
	      if(routingTable[j].dest_id == RecvdUpdatePacket -> sender_id && routingTable[j].cost == INFINITY){
		routingTable[j].next_hop = RecvdUpdatePacket -> sender_id;
		routingTable[j].cost = costToNbr;
		routingTable[j].path_len = 1;
		routingTable[j].path[0] = RecvdUpdatePacket -> sender_id;
		routingTable[j].path[1] = myID;
		//printf("type1: myupdate: ind: %d, dest_id :%d, next_hop: %d, cost: %d \n", j, routingTable[j].dest_id, routingTable[j].next_hop, routingTable[j].cost);fflush(stdout);
		ischange = 1;
		break;
	      }
	    } 
	  }
	  else
	    {
	      for(j = 1; j < cur_numRoutes; j++)
		{
		  //printf("my: ind: %d, dest_id :%d, next_hop: %d, cost: %d \n", j, routingTable[j].dest_id, routingTable[j].next_hop, routingTable[j].cost);fflush(stdout);
		  ncost = costToNbr + RecvdUpdatePacket -> route[i].cost;
		  if(ncost > INFINITY){ncost = INFINITY;}
		  //printf("costToNbr: %d, ncost: %d for %d \n", costToNbr, ncost, RecvdUpdatePacket -> route[i].dest_id);fflush(stdout);
		  if(routingTable[j].next_hop == RecvdUpdatePacket -> sender_id && routingTable[j].dest_id == RecvdUpdatePacket -> sender_id && routingTable[j].cost == INFINITY) //restart
		    {
		      routingTable[j].next_hop = RecvdUpdatePacket -> sender_id;
		      routingTable[j].cost = costToNbr;
		      routingTable[j].path_len = 1;
		      routingTable[j].path[0] = RecvdUpdatePacket -> sender_id; 
		      routingTable[j].path[1] = myID;
		      ischange = 1;
		      //printf("type2: myupdate: ind: %d, dest_id :%d, next_hop: %d, cost: %d \n", j, routingTable[j].dest_id, routingTable[j].next_hop, routingTable[j].cost);fflush(stdout);
		      break;
		    }
		  else if(routingTable[j].dest_id == RecvdUpdatePacket -> route[i].dest_id && direct){
		    if((routingTable[j].next_hop == RecvdUpdatePacket -> sender_id && routingTable[j].cost != ncost) || (ncost < routingTable[j].cost && myID != RecvdUpdatePacket -> route[i].next_hop))
		      {
			routingTable[j].next_hop = RecvdUpdatePacket -> sender_id;
			routingTable[j].cost = ncost;
			routingTable[j].path_len = RecvdUpdatePacket -> route[i].path_len + 1;
			memcpy(routingTable[j].path ,RecvdUpdatePacket -> route[i].path, (RecvdUpdatePacket -> route[i].path_len + 1) * sizeof(int));
			routingTable[j].path[routingTable[j].path_len] = myID;
			//printf("type3: myupdate: ind: %d, dest_id :%d, next_hop: %d, cost: %d \n", j, routingTable[j].dest_id, routingTable[j].next_hop, routingTable[j].cost);fflush(stdout);
			ischange = 1;
		      }
		    break;
		  }// my neighnbor tells me about other nodes
		}
	      if(numRoutes < MAX_ROUTERS && j == cur_numRoutes && RecvdUpdatePacket -> route[i].dest_id != myID && direct)
		{ 
		  routingTable[numRoutes].dest_id = RecvdUpdatePacket -> route[i].dest_id;
		  routingTable[numRoutes].next_hop =  RecvdUpdatePacket -> sender_id;
		  routingTable[numRoutes].cost = costToNbr + RecvdUpdatePacket -> route[i].cost;
		  routingTable[numRoutes].path_len = RecvdUpdatePacket -> route[i].path_len + 1;
		  memcpy(routingTable[numRoutes].path ,RecvdUpdatePacket -> route[i].path, (RecvdUpdatePacket -> route[i].path_len + 1) * sizeof(int));
		  routingTable[numRoutes].path[routingTable[numRoutes].path_len] = myID;
		  ischange = 1;
		  //printf("type4: myupdate: ind: %d, dest_id :%d, next_hop: %d, cost: %d \n", numRoutes, routingTable[numRoutes].dest_id, routingTable[numRoutes].next_hop, routingTable[numRoutes].cost);fflush(stdout);
		  numRoutes++; 
		}   
	    }
	}
      } 
    }  	    
  return ischange;
}

void ConvertTabletoPkt(struct pkt_RT_UPDATE *UpdatePacketToSend, int myID)
{
  UpdatePacketToSend -> sender_id = myID;
  UpdatePacketToSend -> no_routes = numRoutes;
  memcpy(&(UpdatePacketToSend -> route), &routingTable, sizeof(routingTable));
}

void PrintRoutes(FILE* Logfile, int myID)
{
  int i = 0;
  fputs("\nRouting Table: \n", Logfile);
  for(i = 0; i < numRoutes; i++)
    {
      fprintf(Logfile, "R%d -> R%d: R%d, %d\n", myID, routingTable[i].dest_id, routingTable[i].next_hop, routingTable[i].cost);
    }
  fflush(Logfile);
}

/*void PrintRoutes(FILE* Logfile, int myID)
{
  int i = 0;
  int j = 0;
  fputs("\nRouting Table: \n", Logfile);
  for(i = 0; i < numRoutes; i++)
    {
      fprintf(Logfile, "R%d -> R%d: R%d, %d ", myID, routingTable[i].dest_id, routingTable[i].next_hop, routingTable[i].cost);
      fputs("path: ", Logfile);
      for(j = 0; j <= routingTable[i].path_len; j++){
	fprintf(Logfile, "%d ", routingTable[i].path[j]);
      }
      fputs("\n", Logfile);
    }
    }*/

void UninstallRoutesOnNbrDeath(int DeadNbr)
{
  int i = 0;
  for(i = 0; i < numRoutes; i++)
    {
      if(routingTable[i].next_hop == DeadNbr)
	{
	  routingTable[i].cost = INFINITY;
	  routingTable[i].path_len = 0;
	  memset(routingTable[i].path, 0, sizeof(routingTable[i].path));
	}
    }
}
