#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <sys/time.h>
#include <time.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "ne.h"
#include "router.h"

struct NbrDeathCheckTimer
{
  unsigned int nbr_id;
  unsigned int valid;
  unsigned int isreceived;
  int original;
  int cost;
  struct timeval lTime;
  struct timeval cTime;
}dtimer[MAX_ROUTERS];
int num_routes;
int myID;

struct timeval initimer;
struct timeval cgtimer;
struct timeval current;

int open_clientfd(int clientport)
{
  int clientfd;
  struct sockaddr_in clientaddr;
  typedef struct sockaddr SA;
  //create a socket
  if((clientfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
      return -1;
    }

  //for receiving packet from server
  bzero((char *) &clientaddr, sizeof(clientaddr));
  clientaddr.sin_family = AF_INET;
  clientaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  clientaddr.sin_port = htons((unsigned short) clientport);
  if(bind(clientfd, (SA *)&clientaddr, sizeof(clientaddr)) < 0)
    {
      return -2;
    }
  return clientfd;
}

void InitTimer(struct pkt_INIT_RESPONSE * InitResponse)
{
  int i =0;
  num_routes = InitResponse -> no_nbr;
  for(i = 0; i < num_routes; i++)
    {
      dtimer[i].nbr_id = InitResponse -> nbrcost[i].nbr;
      dtimer[i].valid = FAILURE_DETECTION;
      dtimer[i].cost = InitResponse -> nbrcost[i].cost;
      dtimer[i].original = InitResponse -> nbrcost[i].cost;
      gettimeofday(&dtimer[i].lTime, NULL);
      gettimeofday(&dtimer[i].cTime, NULL);
    }
  gettimeofday(&initimer, NULL);
  gettimeofday(&cgtimer, NULL);
  gettimeofday(&current, NULL);
}
void UpdateNextHop(struct pkt_RT_UPDATE * UpdatePacketToSend)
{
  int i = 0;
  int j = 0;
  for(i = 0; i < num_routes; i++){
    for(j = 0; j < UpdatePacketToSend -> no_routes; j++){
      if(UpdatePacketToSend -> route[j].dest_id == dtimer[i].nbr_id && UpdatePacketToSend -> route[j].dest_id != UpdatePacketToSend -> route[j].next_hop && UpdatePacketToSend -> route[j].cost ==  INFINITY && dtimer[i].valid){
	UpdatePacketToSend -> route[j].next_hop = dtimer[i].nbr_id;
	dtimer[i].cost = dtimer[i].original;
	UpdatePacketToSend -> route[j].cost = dtimer[i].original;
	UpdatePacketToSend -> route[j].path_len = 1;
	UpdatePacketToSend -> route[j].path[0] = dtimer[i].nbr_id;
	UpdatePacketToSend -> route[j].path[1] = myID;
	//printf("FIND NEW ROUTE TO NEIGHNBOR: UpdatePacketToSend -> route[j].next_hop = %d, UpdatePacketToSend -> route[j].cost = %d\n",	UpdatePacketToSend -> route[j].next_hop = dtimer[i].nbr_id, UpdatePacketToSend -> route[j].cost = dtimer[i].original);
      }
    }
  }
}
void UpdateTimer(int sender_id, int isreceived)
{
  int i = 0;
  for(i = 0; i < num_routes; i++)
    {
      if(dtimer[i].isreceived){
	dtimer[i].valid = FAILURE_DETECTION;
	dtimer[i].isreceived = 0;
      }
      else {
	if(dtimer[i].valid != 0){
	  dtimer[i].valid--;
	}
      }
    }
}

int GetCostToNbr(struct pkt_RT_UPDATE * RecvdUpdatePacket)
{
  int i = 0;
  for(i = 0; i < RecvdUpdatePacket -> no_routes; i++){
    if(RecvdUpdatePacket -> route[i].dest_id == myID)
      {
	return RecvdUpdatePacket -> route[i].cost;
      }
  }
  return INFINITY;
}

int checkDead()
{
  int i = 0;
  int ischange = 0;
  for(i=0;i<num_routes;i++)
    {
      if(dtimer[i].valid == 0 && dtimer[i].cost != INFINITY){
	UninstallRoutesOnNbrDeath(dtimer[i].nbr_id);
	dtimer[i].cost = INFINITY;
	ischange = 1;
	//printf("DEADDEADDEADDEADDEADDEAD!     nbr_id: %d\n", dtimer[i].nbr_id);
      }
    } 
  return ischange;
}

int main(int argc, char ** argv){
  int clientfd, clientport;

  struct hostent *hp;
  struct sockaddr_in serveraddr;
  socklen_t serverlen = sizeof(serveraddr);;

  int bytes_read;
  fd_set rfds;
  int retval;
  struct timeval tv;

  struct pkt_INIT_REQUEST InitRequest;
  struct pkt_INIT_RESPONSE InitResponse;
  struct pkt_RT_UPDATE UpdatePacketToSend, RecvdUpdatePacket, tmp_packet;
 
  int ischange = 0;
  int costToNbr = 0;
  int isreceived = 0;
  FILE * Logfile;
  char filename[11];
  sprintf(filename, "router%s.log", argv[1]);
  int i = 0;
  int converged = 0;

  myID = atoi(argv[1]);
  clientport = atoi(argv[4]);
  clientfd = open_clientfd(clientport);

  //for sending out packet from client
  if((hp = gethostbyname(argv[2])) == NULL)
    {
      return -1;
    }
  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  bcopy((char *)hp ->h_addr,
	(char *) &serveraddr.sin_addr.s_addr, hp->h_length);
  serveraddr.sin_port = htons(atoi(argv[3])); //argv[3] = hostport

  //Init Request
  InitRequest.router_id = htonl(atoi(argv[1]));
  bytes_read = sendto(clientfd, &InitRequest, sizeof(InitRequest), 0, (struct sockaddr*) &serveraddr, sizeof(serveraddr));
  bytes_read = recvfrom(clientfd, &InitResponse, sizeof(InitResponse), 0, (struct sockaddr*)&serveraddr, &serverlen);
  if(bytes_read)
    {
      ntoh_pkt_INIT_RESPONSE(&InitResponse);
      InitRoutingTbl(&InitResponse, myID);
      InitTimer(&InitResponse);
    }
  Logfile = fopen(filename, "w");
  PrintRoutes(Logfile, myID);
  fclose(Logfile);
  
  tv.tv_sec = UPDATE_INTERVAL; tv.tv_usec= 0;
  //update routing table periodically
  while(1){
    //set up select
    FD_ZERO(&rfds);
    FD_SET(clientfd, &rfds);

    retval = select(clientfd + 1, &rfds, NULL, NULL, &tv);
    if(retval == -1)
      {
	perror("select()");
      }
    if(retval == 0)
      {
	tv.tv_sec = UPDATE_INTERVAL; tv.tv_usec = 0;
	ConvertTabletoPkt(&UpdatePacketToSend, myID);
	UpdateNextHop(&UpdatePacketToSend);
	for(i = 0; i < num_routes;i++)
	  {
	    UpdatePacketToSend.dest_id = dtimer[i].nbr_id;
	    hton_pkt_RT_UPDATE(&UpdatePacketToSend);
	    bytes_read = sendto(clientfd, &UpdatePacketToSend, sizeof(UpdatePacketToSend), 0, (struct sockaddr*) &serveraddr, sizeof(serveraddr));
	    ntoh_pkt_RT_UPDATE(&UpdatePacketToSend);
	  }
	UpdateTimer(RecvdUpdatePacket.sender_id, isreceived);
	isreceived = 0;
	ischange = checkDead();
	if(ischange)
	  {
	    //printf("ISCHANGE2.\n");
	    Logfile = fopen(filename, "a");
	    PrintRoutes(Logfile, myID);
	    fclose(Logfile);
	    gettimeofday(&cgtimer, NULL);
	    converged = 0;
	    //printf("nbr_id: %d, valid: %d, cost: %d\n", dtimer[i].nbr_id, dtimer[i].valid, dtimer[i].cost);
	  }
	gettimeofday(&current, NULL);
	if(difftime(current.tv_sec, cgtimer.tv_sec) > CONVERGE_TIMEOUT && converged == 0)
	  {
	    Logfile = fopen(filename, "a");
	    fprintf(Logfile, "%d: Converged\n", (int)(difftime(current.tv_sec, initimer.tv_sec)));
	    fclose(Logfile);
	    converged = 1;
	    //printf("Converged!!!");
	  }
      }
    else
      {
	//receive sth
	bytes_read = recvfrom(clientfd, &tmp_packet, 1024, 0, (struct sockaddr*)&serveraddr, &serverlen);
	if(bytes_read == sizeof(struct pkt_RT_UPDATE))
	  {
	    memcpy(&RecvdUpdatePacket, &tmp_packet, bytes_read);
	    ntoh_pkt_RT_UPDATE(&RecvdUpdatePacket);
	    costToNbr = GetCostToNbr(&RecvdUpdatePacket);
	    ischange = UpdateRoutes(&RecvdUpdatePacket, costToNbr, myID);
	    if(ischange)
	      {
		//printf("ISCHANGE1.\n");
		Logfile = fopen(filename, "a");
		PrintRoutes(Logfile, myID);
		fclose(Logfile);
		gettimeofday(&cgtimer, NULL);
		converged = 0;
	      }
	     for(i = 0; i < num_routes; i++)
	       {
		 if(dtimer[i].nbr_id == RecvdUpdatePacket.sender_id)
		   {
		     dtimer[i].isreceived = 1;
		   }
	       }
	  }
      }
  }
  return 0;
}
